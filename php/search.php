<?php  
set_time_limit(0);

header("Content-type:application/json;Charset=utf8");

header('Access-Control-Allow-Origin:*');

header('Access-Control-Allow-Methods:GET');

header('Access-Control-Allow-Headers:x-requested-with');

define('PATH', strtr(__DIR__, '\\', '/'));

require(PATH.'/init.php');

$str = isset($_GET['str']) ? $_GET['str'] : "千百度";

echo search($str);
